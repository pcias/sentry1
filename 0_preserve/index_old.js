var http = require('http');
var querystring=require('querystring');
var url=require('url');
var fs = require('fs');

var port = process.env.PORT; 
//var port = 8810; 

var dev = '/dev/ttyACM0';
var stream = fs.createWriteStream(dev, { flags : 'w' });

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  
  var pathname = url.parse(req.url).pathname;
  if(pathname==='/c') {
      var motor = querystring.parse(url.parse(req.url).query)['motor'];
      var command = querystring.parse(url.parse(req.url).query)['command'];
      var comparam = querystring.parse(url.parse(req.url).query)['comparam'];
      
      var issue = 'c'+motor+command+comparam;
      
      stream.write(issue);
      console.log(issue);
      res.end(issue+' OK\n');
  }
  else {
        //fall back to paperboy
        var paperboy = require('paperboy');
        
        var ip = request.connection.remoteAddress;
        var path = require('path');
        var webroot = path.join(__dirname, '');

        paperboy
        .deliver(webroot, request, response)
        .addHeader('X-PaperRoute', 'Node')
        .before(function() {
            //console.log('Request received for ' + request.url);
        })
        .after(function(statusCode) {
            //console.log(statusCode + ' - ' + request.url + ' ' + ip);
        })
        .error(function(statusCode, msg) {
            //console.log([statusCode, msg, request.url, ip].join(' '));
            response.writeHead(statusCode, {
                'Content-Type': 'text/plain'
            });
            response.end('Error [' + statusCode + ']');
        })
        .otherwise(function(err) {
            //console.log([404, err, request.url, ip].join(' '));
            response.writeHead(404, {
                'Content-Type': 'text/plain'
            });
            response.end('Error 404: File not found');
        });
  }
}).listen(port);
